const {Note} = require('./models/Notes')
const User = require('./models/Users')
const mongoose = require("mongoose");
const {loginUser} = require("./usersService");

function createNote(req, res) {
    const {text} = req.body
    if (!text) {
        res.status(400).send({message: "please enter a text"})
        return
    }
    const note = new Note({
        text,
        userId: req.user.userId,
    })
    note.save().then((saved) => {
        res.json({message: 'Success!'});

    })
}

async function getNote(req, res) {
    if ((!mongoose.Types.ObjectId.isValid(req.params.id))) {
        res.status(400).send({message: "wrong id"})
        return
    }
    return Note.findById(req.params.id)
        .then((note) => {
            console.log(req.user.userId, note.userId.toString())
            if (req.user.userId === note.userId.toString()) {
                res.status(200).send({
                    "note": note,
                })
                return
            } else {
                res.status(400).send({message: "wrong id"})
            }
        })
}

function getNotes(req, res) {
    return Note.find().then((result) => {
        let arr = []
        result.forEach((element) => {
            if (req.user.userId === element.userId.toString()) {
                arr.push(element)
            }
        })
        res.status(200).send({
            "offset": 0,
            "limit": 0,
            "count": 0,
            "notes": arr
        })
    });

}

async function putNote(req, res) {
    const note = await Note.findById(req.params.id);
    const {text} = req.body;

    if (text) note.text = text;

    return note.save().then((saved) => {
        if (req.user.userId === saved.userId.toString()) {
            res.status(200).send({
                "message": "Success"
            })
            return
        } else {
            res.status(400).send({message: "wrong id"})
        }
    });
}

async function patchNote(req, res) {
    const note = await Note.findById(req.params.id);
    note.completed = !note.completed;
    return note.save().then((saved) => {
        if (req.user.userId === saved.userId.toString()) {
            res.status(200).send({
                "message": "Success"
            })
            return
        } else {
            res.status(400).send({message: "wrong id"})
        }
    });
}

function deleteNote(req, res) {
    return Note.findByIdAndDelete(req.params.id)
        .then((note) => {
            if (req.user.userId === note.userId.toString()) {
                res.send({
                    "message": "Success"
                });
                return;
            } else {
                res.status(400).send({message: "wrong id"})
            }
        });

}

module.exports = {
    createNote,
    getNote,
    getNotes,
    putNote,
    patchNote,
    deleteNote
}
