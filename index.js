const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://dima:33333@cluster0.3r9b82o.mongodb.net/?retryWrites=true&w=majority');

const { notesRouter} = require('./notesRouter.js');
const { usersRouter } = require('./usersRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api', usersRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
