const mongoose = require('mongoose');

const schema = mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    completed: {
        type: Boolean,
        required: true,
        default: false
    },
    text: {
        type: String,
        required: true
    }
}, {timestamps:{updatedAt: false, createdAt: "createdDate"}});


let Note = mongoose.model('Note', schema)
module.exports = {
    Note
}
